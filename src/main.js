import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import serviceRegistry from './components/services/ServiceRegistry.js'
import '@aws-amplify/ui-vue';
import Amplify from 'aws-amplify';
import router from './router'

Vue.use(VueRouter);
Vue.config.productionTip = false;

export const eventBus = new Vue();

new Vue({
  provide: serviceRegistry,
  render: h => h(App),
  router
}).$mount('#app');

Amplify.configure({
  Auth: {
    region: 'us-east-2',
    userPoolId: 'us-east-2_0B48p93kB',
    userPoolWebClientId: '22nk4taf71n53katbhtm0ei4gc',
    mandatorySignIn: true,
    cookieStorage: {
      domain: process.env.VUE_APP_AUTH_COOKIE_DOMAIN,
      path: '/',
      expires: 365,
      sameSite: "lax",
      secure: false
    },
    authenticationFlowType: 'USER_PASSWORD_AUTH'
  }
});