import axios from 'axios';
import { Auth } from 'aws-amplify'

async function buildHeaders() {
  const user = await Auth.currentAuthenticatedUser();
  const token = user.signInUserSession.idToken.jwtToken;
  return {
    headers: {
      'Authorization': token
    }
  };
}

export default {
  async findAll() {
    let request = await buildHeaders();
    request['params'] = {
      'type': 'UNKNOWN'
    };
    return axios.get(`${process.env.VUE_APP_TAG_SERVICE_HOST}/api/tags`, request).then(response => { return response.data });
  },
  async updateTagType(tagName, tagType) {
    let headers = await buildHeaders();
    return axios.put(`${process.env.VUE_APP_TAG_SERVICE_HOST}/api/tags/${tagName}/type/${tagType}`, null, headers).then(response => { return response.data });
  }
}

