import VueRouter from 'vue-router'
import { Auth } from 'aws-amplify'

import TagsAdminHome from './components/tags-admin/Home'
import Home from './components/Home'
import AuthComponent from './components/Auth'

const routes = [
  { path: '/', component: Home, meta: { requiresAuth: true} },
  { path: '/auth', component: AuthComponent },
  { path: '/tags', component: TagsAdminHome, meta: { requiresAuth: true} }
];

const router = new VueRouter({
  routes
});

router.beforeResolve((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    Auth.currentAuthenticatedUser().then(() => {
      next()
    }).catch(() => {
      next({
        path: '/auth'
      });
    });
  }
  next()
});

export default router;